var mod_reflow;
var mod_hide_fast;
var mod_hide_slow;

$(document).ready(() => {
  /////////////////////////////////////
  // ANVIL MENU ///////////////////////
  /////////////////////////////////////
  $('#logo').on('click', ev => {
    ui.menu.toggle();
  });

  /////////////////////////////////////
  // CHAT POPUPS //////////////////////
  /////////////////////////////////////
  let $chat_markup = $('<div id="chat-popups" class="chat-empty"><a class="button chat-close"><i class="fas fa-times"></i><div class="visually-hidden">Close popup messages.</div></a><div class="chat-log"><ol class="chat-messages"></ol></div></div>');
  $('#players').after($chat_markup);

  // Give chat a little bit longer to join the DOM.
  setTimeout(() => {
    // Listen for new chat messages.
    $('body').arrive('#chat-log .message', function() {
      // If we're currently viewing chat, exit early.
      if ($('#chat').hasClass('active')) {
        return;
      }

      // Get chat messages region.
      let $chat = $(this);
      let $chat_popups = $('#chat-popups');
      let $chat_messages = $chat_popups.find('.chat-messages');

      // Append the latest message.
      $chat_messages.append('<li class="message">' + $chat.html() + '</li>');
      $chat_popups.removeClass('chat-empty');
      $chat_popups.removeClass('chat-hide');

      // Clear old timeouts.
      clearTimeout(mod_hide_fast);
      clearTimeout(mod_hide_slow);

      // Trigger reflow;
      $chat_popups.removeClass('chat-show');
      mod_reflow = setTimeout(() => {
        $chat_popups.addClass('chat-show');
      }, 1);

      // Hide the popup manually.
      $chat_popups.find('.chat-close').on('click', function(event) {
        // Add the class to animate the hide.
        $chat_popups.addClass('chat-hide');

        // Remove markup and classes after 250ms.
        mod_hide_fast = setTimeout(() => {
          $chat_popups.removeClass('chat-hide');
          $chat_popups.addClass('chat-empty');
          $chat_messages.html('');
        }, 250);
      });

      // If no other action was performed, remove markup and
      // classes after 5s.
      mod_hide_slow = setTimeout(() => {
        $chat_popups.removeClass('chat-show');
        $chat_popups.addClass('chat-empty');
        $chat_messages.html('');
      }, 5000);
    });
    // End our manual delay.
  }, 2000);

  /////////////////////////////////////
  // WINDOW HEIGHT ////////////////////
  /////////////////////////////////////
  $('body').arrive('.window-app', function () {
    let $dialog = $(this);
    // If this dialog is larger than the window, add a class to it.
    if ($dialog.outerHeight(true) > window.innerHeight) {
      $dialog.addClass('mod-short');
    }
  });
});
