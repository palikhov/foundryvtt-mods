# Install

1. Add the included `css` directory to your `/public/worlds/WORLD` directory.
2. Add the included `scripts` directory to your `/public/worlds/WORLD` directory.
3. Add the following scripts and styles entries to your `/public/worlds/WORLD/world.json` file somewhere in the main `{` braces, as shown in the included example.world.json file. Replace "WORLD" with your world's folder name.
4. Restart Foundry VTT.

```json
  "scripts": [
    "/worlds/WORLD/scripts/arrive.min.js",
    "/worlds/WORLD/scripts/mod-anvil.js"
  ],
  "styles": [
    "/worlds/WORLD/css/mod-anvil.css"
  ]
```

# Uninstall

Delete the files added in the install step and restart Foundry VTT.

# Current Features

## Anvil Menu

Click the Anvil logo will open up the player menu, in addition to the default behavior (escape key).

## Chat Notifications

![Screenshot of chat notifications](https://i.imgur.com/CZpa0Iv.jpg)

A very simple notifications area that will display chat log messages in the bottom left corner of your screen if a new message comes through and you were not already viewing the chat log. Because Foundry is unaware of this notifications area, interactive elments (buttons, icons) have been hidden.

Notifications can be manually closed, or they will automatically close after 5 seconds. There isn't currently a setting to opt-out of notifications, but that feature is planned.

## Maximum Height On Character Sheets

![Screenshot of window height](https://i.imgur.com/mzgYQiE.jpg)

Added a height check to compare any pop-up windows to see if they're larger than the browser viewport. If they are larger (usually character sheets on a small monitor), a special class is applied to them to reduce their maximum height to 80% of the screen and position them 10% down from the top of the screen.